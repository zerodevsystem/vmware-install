# VMware-install

Dica de como instalar o WMware 16.2.1 Pro no POP OS Linux 21.10

## Getting started

Faça o dowload do arquivo VMware-Workstation-Full-16.2.1-18811642.x86_64.bundle [aqui](https://www.vmware.com/go/getworkstation-linux) ou diretamente no site do fabricante: https://www.vmware.com/br/products/workstation-pro/workstation-pro-evaluation.html

## Installation

1.  Abra o Terminal e acesse o diretório onde está o arquivo .bundle baixado. Caso esteja na pasta Downloads acesse assim:
```
cd ~/Downloads
```
2.  Utilize o comando chmod para atribuir permissão de execução ao arquivo .bundle
```
chmod +x VMware-Workstation-Full-16.2.1-18811642.x86_64.bundle
```
3.  Execute o arquivo para iniciar a instalação
```
./VMware-Workstation-Full-16.2.1-18811642.x86_64.bundle
```

## Usage

Você pode abrir o programa pelo menu gráfico ou pelo terminal com o comando:

```
vmware
```

## Support

Nem sempre a instalação é tão simples assim. Às vezes pode acontecer do seu SO não apresentar todos os requisitos de software necessários. Um dos erros que tive que corrigir foi em relação aos módulos [vmmon] e [vmnet]

### Modulos vmmon e vmnet

Para instalar os módulos necessários para a execução do VMware baixe o arquivo workstation-16.2.1.tar.gz disponibilizado [aqui](https://gitlab.com/zerodevsystem/vmware-install.git/workstation-16.2.1.tar.gz), descompacte o arquivo, acesse o diretório, copie os arquivos para os diretórios específicos e reconfigure a instalação. É muito simples. Execute os passos abaixo:

##### baixe os arquivos
[Opção 1]: 
```
wget https://gitlab.com/zerodevsystem/vmware-install.git/workstation-16.2.1.tar.gz
```

[Opção 2]:
```
wget https://github.com/mkubecek/vmware-host-modules/archive/workstation-16.2.1.tar.gz
```
Descompacte o arquivo:
```
tar -zxf workstation-16.2.1.tar.gz 
```
Acesse a pasta descompactada
```
cd vmware-host-modules-workstation-16.2.1/
```
Crie os arquivo tar vmmon-only e vmnet-only
```
tar -cf vmmon.tar vmmon-only
```
```
tar -cf vmnet.tar vmnet-only
```
Copie os arquivos para a pasta dos módulos do VMware
```
cp -v vmmon.tar /usr/lib/vmware/modules/source

```
```
sudo cp -v vmmon.tar /usr/lib/vmware/modules/source`
```
Reconfigure
```  
sudo vmware-modconfig --console --install-all
```
Pronto. Pode executar o programa
```
vmware
```

## Contributing
Baseado na experiência de [mkubecek](https://github.com/mkubecek/)

## Authors and acknowledgment
Fábio Linhares: zerodevsytem@gmail.com
## License
Copyright (C) 2022 [Free Software Foundation, Inc.](https://fsf.org/). Everyone is permitted to copy and distribute verbatim copies of this project and all documents, changing it is allowed.


## Project status
Active